import * as firebase from 'firebase';

var config = {
	apiKey: 'AIzaSyBm8S1GHi5lNPQtv8caVvDop6YZn7lCgLc',
	authDomain: 'sollida-39b00.firebaseapp.com',
	databaseURL: 'https://sollida-39b00.firebaseio.com',
	projectId: 'sollida-39b00',
	storageBucket: 'sollida-39b00.appspot.com',
	messagingSenderId: '611245675834'
};

const firebaseApp = firebase.initializeApp(config);

export default firebaseApp;
