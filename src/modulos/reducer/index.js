/*
  Estrutura combinada Redux, status indispensáveis serão gerenciados
  aqui, injetados no router em index.js na raiz do documento.
*/
'use strict';

import { createStore, combineReducers } from 'redux';

// Ações
const incrementar = { type: 'INCREMENTAR' };
const decrementar = { type: 'DECREMENTAR' };
const ativar = { type: 'ATIVAR' };
const desativar = { type: 'DESATIVAR' };

//States
const Sollidainfos = {
	tokens: 0
};

function updateTokens({ tokens }) {
	return {
		type: 'INCREMENTAR',
		tokens
	};
}

// Reducer (Contador)
const maisTokens = (state = Sollidainfos, action) => {
	switch (action.type) {
		case 'INCREMENTAR':
			return { tokens: action.tokens };
		default:
			return state;
	}
};
// Reducer (Contador)
//SEM UTILIDADE NO MOMENTO
const menosTokens = (state = Sollidainfos, action) => {
	switch (action.type) {
		case 'INCREMENTAR':
			return { tokens: Sollidainfos.tokens + 1 };
		case 'DECREMENTAR':
			return { tokens: Sollidainfos.tokens - 1 };
		default:
			return state;
	}
};

//Combining Reducers
const rootReducer = combineReducers({
	menosTokens,
	maisTokens
});

//Creating Store
const store = createStore(rootReducer);

//utils
const utils = (type, data) => {
	switch (type) {
		case 0:
			return store.dispatch(updateTokens({ tokens: data }));
			break;
		case 1:
			return store.getState();
		default:
	}
};

export { store, utils };
