import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { Router, Route, hashHistory, IndexRoute } from 'react-router';
import firebaseApp from './modulos/firebase/config';
import App from './app';
import Login from './paginas/login';
import Forgot from './paginas/forgot';
import Register from './paginas/register';
import Commom from './paginas/dashboard/commom';
import Home from './paginas/dashboard/home';
import Share from './paginas/dashboard/share';
import History from './paginas/dashboard/history';
//Redux

ReactDOM.render(
	<Router history={hashHistory}>
		<Route path="/" component={Commom}>
			>
			<IndexRoute
				component={Home}
				firebase={firebaseApp}
				history={hashHistory}
			/>
			<Route
				path="/referral"
				component={Share}
				firebase={firebaseApp}
				history={hashHistory}
			/>
			<Route
				path="/transactions"
				component={History}
				firebase={firebaseApp}
				history={hashHistory}
			/>
		</Route>
		<Route component={App}>
			<Route
				path="/login"
				component={Login}
				firebase={firebaseApp}
				history={hashHistory}
			/>
			<Route
				path="/forgot"
				component={Forgot}
				firebase={firebaseApp}
				history={hashHistory}
			/>
			<Route
				path="/register"
				component={Register}
				firebase={firebaseApp}
				history={hashHistory}
			/>
		</Route>
	</Router>,
	document.getElementById('root')
);
registerServiceWorker();
