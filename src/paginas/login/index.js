import React, { Component } from 'react';
import firebaseApp from '../../modulos/firebase/config';
import { ToastContainer, toast } from 'react-toastify';
import { Animated } from 'react-animated-css';
import 'react-toastify/dist/ReactToastify.css';

import './assets/css/login.css';
import { Link } from 'react-router/lib';
// import { hashHistory } from 'react-router';

class Login extends Component {
	constructor(props) {
		super(props);

		this.state = {
			emailorUsername: '',
			password: '',
			isLoading: false
		};

		this.auth = this.auth.bind(this);
	}

	notifyError = message => {
		toast.error(message, {
			position: 'top-right',
			autoClose: 3000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true,
			draggablePercent: 60
		});
	};

	//validar os campos do formulário
	isValidForm = () => {
		let isValid = true,
			emailorUsername = this.state.emailorUsername,
			password = this.state.password;

		let requiredRegex = /^.{1,}$/;

		let spacesRegex = /\s/g;

		if (!requiredRegex.test(emailorUsername.replace(spacesRegex, ''))) {
			this.notifyError('Email or Username field is required');
			isValid = false;
		}

		if (!requiredRegex.test(password.replace(spacesRegex, ''))) {
			this.notifyError('Password field is required');
			isValid = false;
		}

		return isValid;
	};

	auth = event => {
		event.preventDefault();

		this.setState({ isLoading: true });

		if (!this.isValidForm()) {
			this.setState({ isLoading: false });
			return;
		}

		let { history } = this.props.route;

		let _this = this,
			emailorUsername = this.state.emailorUsername,
			password = this.state.password;

		firebaseApp
			.auth()
			.signInWithEmailAndPassword(emailorUsername, password)
			.then(() => {
				history.push('/');
				return;
			})
			.catch(function(error) {
				// var errorMessage = error.message;

				_this.notifyError('Authentication failed');
				_this.setState({ isLoading: false });
				return;
			});
	};

	render() {
		return (
			<div className="container-fluid">
				<ToastContainer
					position="top-right"
					autoClose={3000}
					hideProgressBar={false}
					newestOnTop={false}
					closeOnClick
					rtl={false}
					pauseOnVisibilityChange
					draggable
					pauseOnHover
					draggablePercent={60}
				/>
				<div className="row justify-content-center align-items-center">
					<div className="col-xl-4 col-lg-5 col-md-6 col-sm-1">
						<Animated
							animationIn="fadeIn"
							animationOut="fadeOut"
							isVisible={true}
						>
							<div className="loginCard">
								<div className="row justify-content-center align-items-center mt-40">
									<div className="logo">
										<img alt="" src="./images/logo.png" />
									</div>
								</div>
								<div className="row">
									<div className="col">
										<h4 className="loginlabel mt-20">Login</h4>
									</div>
								</div>
								<form onSubmit={this.auth}>
									<div className="row justify-content-center align-items-center mt-20">
										<div className="col">
											<input
												type="text"
												className="form-control login-control"
												placeholder="Email"
												value={this.state.emailorUsername}
												onChange={e =>
													this.setState({ emailorUsername: e.target.value })
												}
											/>
										</div>
									</div>
									<div className="row justify-content-center align-items-center">
										<div className="col">
											<input
												type="password"
												className="form-control login-control"
												placeholder="Password"
												value={this.state.password}
												onChange={e =>
													this.setState({ password: e.target.value })
												}
											/>
										</div>
									</div>
									<div className="row justify-content-end align-items-end">
										<div className="col">
											<Link className="forgot" to="forgot">
												I forgot my password{' '}
											</Link>
										</div>
									</div>
									<div className="row justify-content-around check mt-10">
										<div className="col-5">
											<div className="form-check ">
												<input
													className="form-check-input"
													type="checkbox"
													value=""
													id="defaultCheck1"
												/>
												<label
													className="form-check-label"
													htmlFor="defaultCheck1"
												>
													Remember me?
												</label>
											</div>
										</div>
										<div className="col-7">
											<Link className="register" to="register">
												Don't have an account?
											</Link>
										</div>
									</div>
									<div className="row justify-content-start align-items-start">
										<div className="col">
											<button
												className="btn btn-lg btn-login mt-10"
												type="submit"
												disabled={this.state.isLoading}
											>
												{this.state.isLoading ? 'Logging in...' : 'Log In'}
											</button>
										</div>
									</div>
								</form>
							</div>
						</Animated>
					</div>
				</div>
			</div>
		);
	}
}

export default Login;
