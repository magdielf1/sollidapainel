import React, { Component } from 'react';
import firebaseApp from '../../../modulos/firebase/config';
import { PaginationData, Pagination } from './component/index';
import './assets/css/history.css';

class History extends Component {
	constructor(props) {
		super(props);
		this.state = {
			requests: [],
			history: true
		};
	}
	getRequestsHistory = () => {
		let _this = this;
		firebaseApp.auth().onAuthStateChanged(function(user) {
			if (user) {
				const ref = firebaseApp.database().ref(`order/${user.uid}`);
				ref.on('value', function(snapshot) {
					var requests = [];
					ref.once('value').then(function(snapshot) {
						let allData = snapshot.val();
						for (let prop in allData) {
							requests = [allData[prop], ...requests];
						}
						if (_this.state.requests !== requests) {
							_this.setState({ requests, history: false });
						}
					});
				});

				var requests = [];
				ref.once('value').then(function(snapshot) {
					let allData = snapshot.val();
					for (let prop in allData) {
						requests = [allData[prop], ...requests];
					}
					if (_this.state.requests !== requests) {
						_this.setState({ requests, history: false });
					}
				});
			}
		});
	};
	componentWillMount() {
		this.setState({ requests: [] });
		this.getRequestsHistory();
	}
	render() {
		const dataVar = this.state.requests;
		return (
			<div className="main">
				<div className="table-responsive">
					<table className="table">
						{dataVar.length ? (
							<Pagination data={dataVar}>
								<PaginationData />
							</Pagination>
						) : (
							<div className="col-8 text-left">
								{this.state.history
									? 'Please wait, loading...'
									: 'No transactions recorded.'}
							</div>
						)}
					</table>
				</div>
				<sub>
					{dataVar.length ? (
						<span>
							*Paypal payments must be confirmed to support@sollida.com <br />*Payments
							using cryptocurrency are automatically confirmed.
						</span>
					) : (
						''
					)}
				</sub>
			</div>
		);
	}
}

export default History;
