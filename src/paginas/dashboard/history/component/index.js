import React, { Component, PropTypes } from 'react';

class Pagination extends Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			currentPage: null,
			pageCount: null
		};
	}

	componentWillMount() {
		const startingPage = this.props.startingPage ? this.props.startingPage : 1;
		const data = this.props.data;
		const pageSize = this.props.pageSize;
		let pageCount = parseInt(data.length / pageSize);
		if (data.length % pageSize > 0) {
			pageCount++;
		}
		this.setState({
			currentPage: startingPage,
			pageCount: pageCount
		});
	}

	setCurrentPage(num) {
		this.setState({ currentPage: num });
	}

	createControls() {
		let controls = [];

		const pageCount = this.state.pageCount;

		for (let i = 1; i <= pageCount; i++) {
			const baseClassName = 'pagination-controls__button';
			const activeClassName =
				i === this.state.currentPage ? `${baseClassName}--active` : '';
			controls.push(
				<div
					className={`${baseClassName} ${activeClassName}`}
					onClick={() => this.setCurrentPage(i)}
				>
					{i}
				</div>
			);
		}
		return controls;
	}

	createPaginatedData() {
		const data = this.props.data;
		const pageSize = this.props.pageSize;
		const currentPage = this.state.currentPage;
		const upperLimit = currentPage * pageSize;
		const dataSlice = data.slice(upperLimit - pageSize, upperLimit);
		return dataSlice;
	}

	render() {
		return (
			<div className=" justify-content-center">
				{React.cloneElement(this.props.children, {
					data: this.createPaginatedData()
				})}

				<div className="pagination-controls justify-content-center">
					{this.createControls()}
				</div>
			</div>
		);
	}
}

Pagination.defaultProps = {
	pageSize: 5,
	startingPage: 1
};

class PaginationData extends React.Component {
	componentWillMount() {}
	render() {
		const data = this.props.data;
		return (
			<div className="table-responsive">
				<table class="table" style={{ backgroundColor: '#eef4ff' }}>
					<thead>
						<tr>
							<th scope="col">ID</th>
							<th scope="col">DATE</th>
							<th scope="col">AMOUNT</th>
							<th scope="col">CURRENCY</th>
							<th scope="col">STATUS</th>
							<th scope="col">TOTAL SLD</th>
							<th scope="col">PAY METHOD</th>
						</tr>
					</thead>
					<tbody>
						{data.map((e, index) => {
							return (
								<tr key={index} style={{ borderBottom: '2px solid #d8d6f6' }}>
									<th scope="row">{e.id}</th>
									<td>{e.created_at}</td>
									<td>{e.price_amount}</td>
									<td>{e.price_currency}</td>
									<td>
										{e.status === 'new' ? (
											<span style={{ color: '#cf740e' }}>waiting</span>
										) : (
											<span style={{ color: '#11b636' }}>paid</span>
										)}
									</td>
									<td>{e.price_amount * 100 * 2}</td>
									<td>
										{e.status === 'new' ? (
											<React.Fragment>
												<div class="row" style={{ marginRight: 0 }}>
													<a href={e.payment_url} target="_blank">
														<button
															class="btn"
															style={{
																cursor: 'pointer',
																padding: 0,
																minWidth: 76,
																lineHeight: 2,
																borderRadius: 46
															}}
														>
															CRYPTOS
														</button>
													</a>
												</div>

												<div class="row" style={{ marginRight: 0 }}>
													<form
														action="https://www.paypal.com/cgi-bin/webscr"
														method="post"
														target="_top"
														style={{
															position: 'relative',
															left: 15
														}}
													>
														<input type="hidden" name="cmd" value="_s-xclick" />
														<input
															type="hidden"
															name="encrypted"
															value="-----BEGIN PKCS7-----MIIHPwYJKoZIhvcNAQcEoIIHMDCCBywCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYA2P2O56G2nLz8MaUQ+L4Nz2WV4yxy+uP178Lw9SN8w29t2Es562kL7Rta6FzBsTkVzSab2sLxrq+x8DepqiHKkTwV2dWKCS/TsnsdA4YuCyaLujtKB2n81CoPrOwc0MgFHSCUcT2Cxi6eatV6mcZMU0kLsB/TwdkQenKnh3wx+FzELMAkGBSsOAwIaBQAwgbwGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQI8Kos871hGaaAgZiLCgYXbYR4DXn7NjOV/Jt5tv7ff8JB8uMDJvnzZSn5th99D7pueX0suM9TJYp+3hITR8rNOv4uK7UehCUpTsxH6S7q4s6Fs3EY2g1cfXjcg433380NHW29XwbsY1Y3cakb9W6Uosdea7etg9XLhvdFuTe2AJ6npnMo0xZa3xj7fqbgqk2gSlZXRQ2b5m8xxeAkmcFUKphbXqCCA4cwggODMIIC7KADAgECAgEAMA0GCSqGSIb3DQEBBQUAMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbTAeFw0wNDAyMTMxMDEzMTVaFw0zNTAyMTMxMDEzMTVaMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbTCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAwUdO3fxEzEtcnI7ZKZL412XvZPugoni7i7D7prCe0AtaHTc97CYgm7NsAtJyxNLixmhLV8pyIEaiHXWAh8fPKW+R017+EmXrr9EaquPmsVvTywAAE1PMNOKqo2kl4Gxiz9zZqIajOm1fZGWcGS0f5JQ2kBqNbvbg2/Za+GJ/qwUCAwEAAaOB7jCB6zAdBgNVHQ4EFgQUlp98u8ZvF71ZP1LXChvsENZklGswgbsGA1UdIwSBszCBsIAUlp98u8ZvF71ZP1LXChvsENZklGuhgZSkgZEwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tggEAMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADgYEAgV86VpqAWuXvX6Oro4qJ1tYVIT5DgWpE692Ag422H7yRIr/9j/iKG4Thia/Oflx4TdL+IFJBAyPK9v6zZNZtBgPBynXb048hsP16l2vi0k5Q2JKiPDsEfBhGI+HnxLXEaUWAcVfCsQFvd2A1sxRr67ip5y2wwBelUecP3AjJ+YcxggGaMIIBlgIBATCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwCQYFKw4DAhoFAKBdMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTE4MDgyOTE2MDkzOVowIwYJKoZIhvcNAQkEMRYEFI0cz0GGS7/OcBQsHXD5zYD+K2YyMA0GCSqGSIb3DQEBAQUABIGAuja6X0Nu0c+DdJQkDVqLXpZgXKJ6Zbm1IEgao2b+y6etef3ayYF4XXRz0AFoDxY6H0qqPTCqlyrD3izmTTtvxh+4Anc8trv16xkKyZUg6gKf2RBEjHPTaUlK+wMPVUucQ4yUv3zMEpgo+s1EUj0CoXJ8WU8Q8gbswGKlLGLcZRI=-----END PKCS7-----"
														/>

														<button
															class="btn"
															type="submit"
															style={{
																cursor: 'pointer',
																padding: 0,
																minWidth: 76,
																lineHeight: 2,
																borderRadius: 46
															}}
														>
															PAYPAL
														</button>

														<img
															alt=""
															border="0"
															src="https://www.paypalobjects.com/pt_BR/i/scr/pixel.gif"
															width="1"
															height="1"
														/>
													</form>
												</div>
											</React.Fragment>
										) : (
											<a href={e.payment_url} target="_blank">
												<img src="./images/ok.png" style={{ width: 26 }} />
												<span style={{ color: '#11b636' }}>Paid</span>
											</a>
										)}
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>
			</div>
		);
	}
}

export { PaginationData, Pagination };
