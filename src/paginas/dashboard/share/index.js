import React, { Component } from 'react';
import Stepper from 'react-stepper-horizontal';
import StepA from './components/stepA';
import StepB from './components/stepB';
import './assets/css/share.css';

class Share extends Component {
	constructor(props) {
		super(props);
		this.state = {
			dropList: 'block',
			activeCurrent: 0
		};
	}

	setStep(e) {
		this.setState({ activeCurrent: e });
	}

	render() {
		return (
			<div
				className="main animated fadeInUp"
				data-animate="fadeInUp"
				data-delay="1.15"
				style={{ visibility: 'visible', animationDelay: ' 0.15s' }}
			>
				<Stepper
					steps={[
						{ title: 'Sollida AirDrop' },
						{ title: 'Submit Documents' },
						{ title: 'Join Telegram' },
						{ title: 'Share FB' },
						{ title: 'Enjoy 2000 SLD!' }
					]}
					activeStep={this.state.activeCurrent}
				/>
				{this.state.activeCurrent === 0 ? (
					<StepA setStep={e => this.setStep(e)} />
				) : (
					<div />
				)}
				{this.state.activeCurrent === 1 ? (
					<StepB setStep={e => this.setStep(e)} />
				) : (
					<div />
				)}
			</div>
		);
	}
}

export default Share;
