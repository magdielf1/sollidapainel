import React, { Component } from 'react';

class airDrop extends Component {
	constructor(props) {
		super(props);
		this.state = {
			teste: 1
		};
	}
	componentWillMount() {}
	render() {
		return (
			<div
				className="main animated fadeInUp"
				data-animate="fadeInUp"
				data-delay="1.15"
				style={{ visibility: 'visible', animationDelay: ' 0.15s' }}
			>
				<div class="card">
					<div class="card-body">
						<div class="container">
							<div class="row">
								<div class="col-sm">
									<p class="lead">
										Join our rewards program and earn <b>2000 SLD</b>! It is a
										simple and quick process, at the end of the ICO period you
										can send the SLD coins to a wallet and start using SLD
										Coins!
									</p>
									<hr class="my-4" />
									<p>
										All requests are manually reviewed by our staff, malicious
										requests will be refused.
									</p>

									<a
										class="btn btn-alt btn-sm "
										role="button"
										onClick={() => this.props.setStep(1)}
									>
										GO!
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default airDrop;
