import React, { Component } from 'react';

class teleGram extends Component {
	constructor(props) {
		super(props);
		this.state = {
			teste: 1
		};
	}
	componentWillMount() {}
	render() {
		return (
			<div
				className="main animated fadeInUp"
				data-animate="fadeInUp"
				data-delay="1.15"
				style={{ visibility: 'visible', animationDelay: ' 0.15s' }}
			>
				<div class="card">
					<div class="card-body">
						<div class="container">
							<div class="row">
								<div class="col-sm">
									<p>
										Precisamos confirmar sua identidade, para remover qualquer
										tentativa de fraude. Não se preocupe, após a verificação os
										documentos serão deletados de nosso banco de dados.
									</p>
									<p>
										É bem simples, apenas tire uma self, segurando o seu
										documento oficial com foto (emitido por um orgão do governo)
										e anexe a foto no formulário abaixo.
									</p>
									{/*File Input*/}
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="inputGroupFileAddon01">
												Upload
											</span>
										</div>
										<div class="custom-file">
											<input
												type="file"
												class="custom-file-input"
												id="inputGroupFile01"
												aria-describedby="inputGroupFileAddon01"
											/>
											<label class="custom-file-label" for="inputGroupFile01">
												Choose file
											</label>
										</div>
									</div>
									{/*File Input*/}
									<br />
									<br />
									<a
										class="btn btn-alt btn-sm "
										role="button"
										onClick={() => this.props.setStep(1)}
									>
										Submit
									</a>
								</div>

								<div class="col-sm">
									<img src="./images/self.png" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default teleGram;
