import React, { Component } from 'react';

class airDrop extends Component {
	constructor(props) {
		super(props);
		this.state = {
			teste: 1,
			file: ''
		};
	}
	base64Img() {
		console.log(this.state.file);
		var xhr = new XMLHttpRequest();
		xhr.open('GET', this.state.file, true);
		xhr.responseType = 'blob';
		xhr.onload = function(e) {
			console.log(this.response);
			var reader = new FileReader();
			reader.onload = function(event) {
				var res = event.target.result;
				console.log(res);
			};
			var file = this.response;
			reader.readAsDataURL(file);
		};
		xhr.send();
	}
	componentWillMount() {}
	render() {
		return (
			<div
				className="main animated fadeInUp"
				data-animate="fadeInUp"
				data-delay="1.15"
				style={{ visibility: 'visible', animationDelay: ' 0.15s' }}
			>
				<div class="card">
					<div class="card-body">
						<div class="container">
							<div class="row">
								<div class="col-sm">
									<p>
										Precisamos confirmar sua identidade, para remover qualquer
										tentativa de fraude. Não se preocupe, após a verificação os
										documentos serão deletados de nosso banco de dados.
									</p>
									<p>
										É bem simples, apenas tire uma self, segurando o seu
										documento oficial com foto (emitido por um orgão do governo)
										e anexe a foto no formulário abaixo.
									</p>
									{/*File Input*/}
									<div class="input-group mb-3">
										<div class="custom-file">
											<input
												type="file"
												class="custom-file-input"
												id="inputGroupFile02"
												value={this.state.file}
												onChange={e => this.setState({ file: e.target.value })}
											/>
											<label
												class="custom-file-label"
												for="inputGroupFile02"
												aria-describedby="inputGroupFileAddon02"
											>
												Choose file
											</label>
										</div>
										<div class="input-group-append">
											<span class="input-group-text" id="inputGroupFileAddon02">
												Upload
											</span>
										</div>
									</div>
									{/*File Input*/}
									<br />
									<br />
									<a
										class="btn btn-alt btn-sm "
										role="button"
										onClick={() => this.base64Img()}
									>
										Submit
									</a>
								</div>

								<div class="col-sm">
									<img src="./images/self.png" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default airDrop;
