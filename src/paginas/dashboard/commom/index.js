import React, { Component } from 'react';

import './assets/css/commom.css';
import './assets/css/btn.scss';
import { Link } from 'react-router/lib';
import firebaseApp from '../../../modulos/firebase/config';
import { hashHistory } from 'react-router';
import LoadingScreen from 'react-loading-screen';
import { Animated } from 'react-animated-css';
import Aside from '../home/components/aside';
import App from '../../../app';
//Reducer
import { store, utils } from '../../../modulos/reducer';

class Commom extends Component {
	state = {
		teste: '',
		logged: 'block',
		loading: false,
		currentUser: null,
		tokens: 0,
		indications: 0
	};

	title() {
		let { pathname } = this.props.location;

		if (pathname === '/') return 'Buy Sollida';
		if (pathname === '/referral') return 'AirDrop';
		if (pathname === '/transactions') return 'History';
	}

	logoutUser = () => {
		firebaseApp
			.auth()
			.signOut()
			.then(
				function () {
					console.log('usuário deslogado!');
				},
				function (error) {
					// An error happened.
				}
			);
	};
	createUserDB = user => {
		// let _this = this;
		// console.log(user);
		firebaseApp
			.database()
			.ref('profile/' + user.uid)
			.set({
				uid: user.uid,
				email: user.email,
				emailVerified: user.emailVerified,
				phoneNumber: user.phoneNumber,
				photoURL: user.photoURL,
				displayName: user.displayName,
				referral: 'null',
				token: 'null',
				CloudF: false,
				lastLogin: Date.now()
			});
	};
	getBalance(user) {
		let _this = this;
		const ref = firebaseApp.database().ref(`SLD/${user.uid}`);
		ref.on('value', function (snapshot) {
			let i = 0;
			const data = snapshot.val();
			for (let props in data) {
				i += data[props].tokens;
			}
			if (snapshot.val()) utils(0, i);
		});
		/*
		var requests = [];
		ref.once('value').then(function(snapshot) {
			if (snapshot.val()) utils(0, snapshot.val().tokens);
		});
		*/
	}
	componentWillMount() {
		let _this = this;
		//detecta se o usuário mudou de status para ''autenticado'' se estiver com status ''não antenticado
		//o usuário será redirecionado para a página de login
		firebaseApp.auth().onAuthStateChanged(function (user) {
			if (!user) {
				hashHistory.push('/login');
			} else {
				setTimeout(() => {
					_this.setState({ loading: false });
				}, 2000);
				_this.setState({ currentUser: user });
				_this.createUserDB(user);
				_this.getBalance(user);
			}
		});
	}

	render() {
		let { loading, currentUser, tokens } = this.state;
		return (
			<div>
				{loading ? (
					<LoadingScreen
						loading={true}
						bgColor="E4EDFF"
						textColor="#676767"
						style={{ width: 60 }}
						logoSrc="./assets/gif/loading.gif"
					>
						<div>Sollida</div>
					</LoadingScreen>
				) : (
						<App>
							<div className="container-fluid">
								<Animated
									animationIn="fadeIn"
									animationOut="fadeOut"
									isVisible={true}
								>
									<div className="row justify-content-center align-items-center">
										<div className="col" style={{ "margin": 20 }}>
											<div className="panelCard">
												<header>
													<nav className="navbar navbar-expand-lg navbar-light ">
														<Link className="navbar-brand" to="/">
															<img
																alt=""
																src="./images/logo.png"
																style={{ width: '60%' }}
															/>
														</Link>
														<button
															className="navbar-toggler"
															type="button"
															data-toggle="collapse"
															data-target="#navbarSupportedContent"
															aria-controls="navbarSupportedContent"
															aria-expanded="false"
															aria-label="Toggle navigation"
														>
															<span className="navbar-toggler-icon icon-menu" />
														</button>

														<div
															className="collapse navbar-collapse"
															id="navbarSupportedContent"
														>
															<ul className="navbar-nav ml-auto">
																<li className="nav-item">
																	<Link className="nav-link" to="/">
																		Buy Sollida
															</Link>
																</li>
																<li
																	className="nav-item"
																	style={{ display: 'none' }}
																>
																	<Link className="nav-link" to="/referral">
																		AirDrop
															</Link>
																</li>
																<li className="nav-item">
																	<Link className="nav-link" to="/transactions">
																		History
																	</Link>
																</li>

																<li className="nav-item dropdown">
																	<Link 
																		className="account dropdown-toggle nav-link"
																		data-hover="dropdown"
																		id="navbarDropdownMenuLink"
																		data-toggle="dropdown"
																		aria-haspopup="true"
																		style={{color: "grey"}}
																		>
																		{ currentUser? currentUser.displayName : '...'}
																	</Link>
																	<div
																		className="dropdown-menu"
																		aria-labelledby="navbarDropdownMenuLink"
																	>
																		<Link
																			className="dropdown-item"
																			to="/"
																			onClick={() => this.logoutUser()}
																		>
																			Logout
																</Link>
																	</div>
																</li>
															</ul>
														</div>
													</nav>
												</header>
												<main>
													<div className="container-fluid">
														<div className="row">
															<div className="col">
																<h6>{this.title()}</h6>
															</div>
														</div>
														<div className="row">
															<div className="col mt-20">
																{React.cloneElement(this.props.children, {
																	currentUser
																})}
															</div>
														</div>
													</div>
												</main>
											</div>
										</div>
									</div>
								</Animated>
							</div>
						</App>
					)}
			</div>
		);
	}
}

export default Commom;
