const requestData = (url, success) => {
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url);
	xhr.onreadystatechange = function() {
		if (xhr.readyState > 3 && xhr.status == 200) {
			success(xhr.responseText);
		}
	};
	xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.send();
	return xhr;
};
//https://us-central1-sollida-39b00.cloudfunctions.net/getTotalTokensSld
export const promessaDataICO = url => {
	return new Promise(function(resolve, reject) {
		requestData(url, data => {
			var obj = JSON.parse(data);
			var value = obj;
			resolve(value);
		});
	});
};

export const promessaDaNewslleter = url => {
	return new Promise(function(resolve, reject) {
		requestData(url, data => {
			var obj = JSON.parse(data);
			resolve(obj);
		});
	});
};
