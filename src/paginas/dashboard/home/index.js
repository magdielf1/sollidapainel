import React, { Component } from 'react';
import Section from './components/section';
import Aside from './components/aside';
import './assets/css/home.css';

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			teste: 1
		};
	}

	render() {
		return (
			<div
				className="main animated fadeInUp"
				data-animate="fadeInUp"
				data-delay="0.15"
				style={{ visibility: 'visible', animationDelay: ' 0.15s' }}
			>
				<Section currentUser={this.props.currentUser} />
				<Aside props={this.props} />
			</div>
		);
	}
}

export default Home;
