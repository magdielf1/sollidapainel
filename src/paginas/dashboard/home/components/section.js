import React, { Component } from 'react';
import firebaseApp from '../../../../modulos/firebase/config';
import axios from 'axios';
import { hashHistory } from 'react-router';
import '../assets/css/section.css';

class Section extends Component {
	constructor(props) {
		super(props);
		this.state = {
			teste: 1,
			value: '',
			bonus: 2, //esse valor deverá ser puxado do firebase, mais tarde.
			minValue: 'none',
			buttom: 'BUY SLD'
		};
	}

	componentWillMount() {
		//detecta se o usuário mudou de status para ''autenticado'' se estiver com status ''não antenticado
		//o usuário será redirecionado para a página de login
		firebaseApp.auth().onAuthStateChanged(function(user) {
			if (!user) {
			} else {
			}
		});
	}

	requestPayment() {
		var { value } = this.state;
		var { currentUser } = this.props;

		if (value < 5) {
			this.setState({ minValue: 'block' });
			return;
		}
		var url = `https://us-central1-sollida-39b00.cloudfunctions.net/addMessage?uid=${
			currentUser.uid
		}&email=${currentUser.email}&order_id=${
			currentUser.uid
		}&price_amount=${value}`;
		this.setState({ buttom: 'Loading...' });
		axios
			.get(url, {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			})
			.then(res => {
				hashHistory.push('/transactions');
			})
			.catch(error => {
				this.setState({ buttom: 'BUY SLD' });
			});
	}

	isdisabled() {
		if (this.props.currentUser) {
			if (this.state.value === '') {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	render() {
		const { value } = this.state;
		return (
			<div className="section">
				<label htmlFor="value" className="value">
					How much do you want to invest?
				</label>
				<div className="input-group mb-3">
					<div className="input-group-prepend">
						<span className="input-group-text" id="basic-addon1">
							USD
						</span>
					</div>
					<input
						type="number"
						placeholder="Enter value here"
						className="form-control home"
						id="value"
						value={value}
						onChange={e => this.setState({ value: e.target.value })}
					/>
				</div>
				<div
					className="alert alert-danger"
					role="alert"
					style={{ display: this.state.minValue }}
				>
					{' '}
					Min Buy per User: US$ 5
				</div>
				<div className="row b-b ">
					<div className="col-8 text-left">Total SLD</div>
					<div className="col-4 text-right">
						<span id="total_token_buy">{this.state.value * 100}</span>
					</div>
				</div>
				<div className="row b-b ">
					<div className="col-8 text-left text-success">Token Bônus:</div>
					<div className="col-4 text-right text-success">
						<span id="total_token_bonus">{(this.state.value * 100) / 2}</span>
					</div>
				</div>
				<div className="row b-b ">
					<div className="col-8 text-left">Total</div>
					<div className="col-4 text-right">
						<span id="total_token">
							{this.state.value * 100 + (this.state.value * 100) / 2}
						</span>
					</div>
				</div>
				{/* <div className="row p-3">
					<label htmlFor="" className="p-2">
						Select a payment method:
					</label>
					<div className="col-12 p-2 convert">
						<input type="radio" name="method" value="BTC" />
						<img
							alt=""
							src="https://tokens.bancryp.com/assets/img/profile.png"
							width="30"
						/>
						<span id="BTC" /> BTC
					</div>
					<div className="col-12 p-2 convert">
						<input type="radio" name="method" value="ETH" />
						<img
							alt=""
							src="https://tokens.bancryp.com/assets/img/profile.png"
							width="30"
						/>
						<span id="ETH" /> ETH
					</div>
					<div className="col-12 p-2 convert">
						<input type="radio" name="method" value="LTC" />
						<img
							alt=""
							src="https://tokens.bancryp.com/assets/img/profile.png"
							width="30"
						/>
						<span id="LTC" /> LTC
					</div>
				</div> */}
				<div className="form-buttons-w text-right">
					<button
						className="mr-2 mb-2 btn btn-buy btn-lg "
						type="button"
						id="btPay"
						disabled={this.isdisabled()}
						onClick={() => {
							this.requestPayment();
						}}
					>
						<span>{this.state.buttom}</span>
						{/* <i className="os-icon os-icon-arrow-right"></i> */}
					</button>
				</div>
			</div>
		);
	}
}

export default Section;
