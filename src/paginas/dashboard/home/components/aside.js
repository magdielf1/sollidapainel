import React, { Component } from 'react';
import { promessaDataICO } from '../lib';
import { store, utils } from '../../../../modulos/reducer';

import '../assets/css/aside.css';

class Aside extends Component {
	constructor(props) {
		super(props);
		this.state = {
			teste: 1,
			tokens: 0,
			suppl: 0
		};
	}
	componentWillMount() {
		let _this = this;
		const currentStore = store.getState().maisTokens.tokens;
		//verifica a atual state da store
		this.setState({ tokens: currentStore });
		//faz um listner para aguardar mudanças da store
		store.subscribe(() => {
			_this.setState({ tokens: store.getState().maisTokens.tokens });
		});
		var countDownDate = new Date('Sep 30, 2018 15:37:25').getTime();
		let x = setInterval(function() {
			const now = new Date().getTime();
			const ts = Math.round(new Date().getTime() / 1000);
			let distance = countDownDate - now;

			let days = Math.floor(distance / (1000 * 60 * 60 * 24));
			let hours = Math.floor(
				(distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
			);
			let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			let seconds = Math.floor((distance % (1000 * 60)) / 1000);
			_this.setState({
				days: days > 10 ? `${days}` : `0${days}`,
				hours: hours > 10 ? `${hours}` : `0${hours}`,
				minutes: minutes > 10 ? `${minutes}` : `0${minutes}`,
				seconds: seconds > 10 ? `${seconds}` : `0${seconds}`
			});
		}, 1000);
		promessaDataICO(
			'https://us-central1-sollida-39b00.cloudfunctions.net/getTotalTokensSld'
		).then(function(result) {
			_this.setState({ suppl: (result * 100) / 75000000 });
		});
	}

	render() {
		return (
			<div className="aside">
				<div className="total">
					<div className="xbanc">
						<span>SLD Balance</span>:
						<h1 className="xbanc">{this.state.tokens}</h1>
					</div>
					<div className="indications">
						<span>AirDrop Balance</span>:
						<h1 className="xbanc">0</h1>
					</div>
				</div>
				<div className="count">
					<div className="container-fluid">
						<div className="row justify-content-center align-items-center text-center">
							<div className="col">
								<div
									className=" countdown-box countdown-header text-center"
									ng-show="lang"
								>
									<span className=" extra-line" />
									<h5
										className="animated fadeInUp"
										data-animate="fadeInUp"
										data-delay="1.85"
										style={{ visibility: 'visible', animationDelay: '1.85s' }}
									>
										ICO IS RUNNING. WILL FIND IN{' '}
									</h5>
									<div
										className="token-countdown d-flex align-content-stretch animated fadeInUp"
										data-animate="fadeInUp"
										data-delay="1.95"
										data-date="2018/12/01"
										style={{ visibility: 'visible', animationDelay: '1.95s' }}
									>
										<div className="col">
											<span className="countdown-time countdown-time-first">
												{this.state.days}
											</span>
											<span className="countdown-text">Days</span>
										</div>
										<div className="col">
											<span className="countdown-time">{this.state.hours}</span>
											<span className="countdown-text">Hours</span>
										</div>
										<div className="col">
											<span className="countdown-time">
												{this.state.minutes}
											</span>
											<span className="countdown-text">Minutes</span>
										</div>
										<div className="col">
											<span className="countdown-time countdown-time-last">
												{this.state.seconds}
											</span>
											<span className="countdown-text">Seconds</span>
										</div>
									</div>
									<div
										className="token-status-bar animated fadeInUp"
										data-animate="fadeInUp"
										data-delay="2.05"
										style={{ visibility: 'visible', animationDelay: '2.05s' }}
									>
										<div
											id="token-status"
											className="token-status-percent"
											style={{ width: `${this.state.suppl}%` }}
										/>
										<span
											className="token-status-point token-status-point-1"
											style={{ left: '10%' }}
										>
											Pre - ICO
										</span>
										<span
											className="token-status-point token-status-point-2"
											style={{ left: '35%' }}
										>
											Soft Cap
										</span>
										<span
											className="token-status-point token-status-point-3"
											style={{ left: '80%' }}
										>
											Hard Cap
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Aside;
