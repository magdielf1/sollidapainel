import React, { Component } from 'react';
import firebaseApp from '../../modulos/firebase/config';
import { ToastContainer, toast } from 'react-toastify';
import { hashHistory } from 'react-router';
import { Animated } from 'react-animated-css';
import 'react-toastify/dist/ReactToastify.css';

import './assets/css/register.css';

class Register extends Component {
	constructor(props) {
		super(props);

		this.state = {
			email: '',
			username: '',
			password: '',
			confirmpass: '',
			isLoading: false
		};

		this.registerAccount = this.registerAccount.bind(this);
	}

	notifyError = message => {
		toast.error(message, {
			position: 'top-right',
			autoClose: 3000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true,
			draggablePercent: 60
		});
	};

	//validar os campos do formulário
	isValidForm = () => {
		let isValid = true,
			email = this.state.email,
			username = this.state.username,
			password = this.state.password,
			confirmpass = this.state.confirmpass;

		let requiredRegex = /^.{1,}$/;
		let minLengthRegex = /^.{6,9}$/;

		let spacesRegex = /\s/g;

		if (requiredRegex.test(email.replace(spacesRegex, ''))) {
			let emailRegex = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
			if (!emailRegex.test(email)) {
				this.notifyError('Email address format invalid');
				isValid = false;
			}
		} else {
			this.notifyError('Email field is required');
			isValid = false;
		}

		if (requiredRegex.test(username.replace(spacesRegex, ''))) {
			if (!minLengthRegex.test(username.replace(spacesRegex, ''))) {
				this.notifyError(
					'Username field requires minimum length of 6 and maximum of 9 characters'
				);
				isValid = false;
			}
		} else {
			this.notifyError('Username field is required');
			isValid = false;
		}

		let passowordRegex = /^(?=.*?[A-Za-z])(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{6,}$/;
		if (requiredRegex.test(password.replace(spacesRegex, ''))) {
			if (!passowordRegex.test(password)) {
				this.notifyError(
					'Password field must contain at least 6 digits, 1 symbol and 1 uppercase letter (no spaces)'
				);
				isValid = false;
			}
		} else {
			this.notifyError('Password field is required');
			isValid = false;
		}

		if (requiredRegex.test(confirmpass.replace(spacesRegex, ''))) {
			if (!passowordRegex.test(confirmpass)) {
				this.notifyError(
					'Confirm Password field must contain at least 6 digits, 1 symbol and 1 uppercase letter (no spaces)'
				);
				isValid = false;
			}
		} else {
			this.notifyError('Confirm Password field is required');
			isValid = false;
		}

		return isValid;
	};

	//função para atualizar o nome de usuário
	updateUser = user => {
		let _this = this;
		var updateDisplay = firebaseApp.auth().currentUser;
		updateDisplay
			.updateProfile({
				displayName: user
			})
			.then(function(user) {
				console.log(user);
				hashHistory.push('/');
			})
			.catch(function(error) {
				_this.notifyError(error);
			});
	};

	//função de registro
	registerAccount = event => {
		event.preventDefault();

		this.setState({ isLoading: true });

		if (!this.isValidForm()) {
			this.setState({ isLoading: false });
			return;
		}

		let { history } = this.props.route;

		let _this = this,
			email = this.state.email,
			password = this.state.password,
			confirmpass = this.state.confirmpass;

		if (password !== confirmpass) {
			this.notifyError('Password and Confirm Password field not match');
			this.setState({ isLoading: false });
			return;
		}

		//chamamos a promise do firebase
		firebaseApp
			.auth()
			.createUserWithEmailAndPassword(email, password)
			.then(user => {
				_this.updateUser(_this.state.username);
				history.push('/');
				return;
			})
			.catch(error => {
				//tratamos o erro
				// var errorMessage = error.message;
				var errorCode = error.code;
				if (errorCode === 'auth/email-already-in-use') {
					this.notifyError('Email already in use');
					this.setState({ isLoading: false });
					return;
				}
			});
	};

	render() {
		// var myuser = firebaseApp.auth().currentUser;
		// console.log(myuser); //ativar isso fará com que seja possível visualizar os dados do usuario
		return (
			<div className="container-fluid">
				<ToastContainer
					toastClassName="dark-toast"
					position="top-right"
					autoClose={3000}
					hideProgressBar={false}
					newestOnTop={false}
					closeOnClick
					rtl={false}
					pauseOnVisibilityChange
					draggable
					pauseOnHover
					draggablePercent={60}
				/>
				<div className="row justify-content-center align-items-center">
					<div className="col-xl-4 col-lg-4 col-md-4 col-sm-1">
						<Animated
							animationIn="fadeIn"
							animationOut="fadeOut"
							isVisible={true}
						>
							<div className="registerCard">
								<div className="row justify-content-center align-items-center mt-40">
									<div className="logo">
										<img alt="" src="./images/logo.png" />
									</div>
								</div>
								<div className="row">
									<div className="col">
										<h4 className="registerlabel mt-20">Sign Up</h4>
									</div>
								</div>
								<form onSubmit={this.registerAccount}>
									<div className="row justify-content-center align-items-center mt-20">
										<div className="col">
											<input
												type="text"
												className="form-control register-control"
												placeholder="Email"
												value={this.state.email}
												onChange={e => this.setState({ email: e.target.value })}
											/>
										</div>
									</div>
									<div className="row justify-content-center align-items-center mt-10">
										<div className="col">
											<input
												type="text"
												className="form-control register-control"
												placeholder="Username"
												value={this.state.username}
												onChange={e =>
													this.setState({ username: e.target.value })
												}
											/>
										</div>
									</div>
									<div className="row justify-content-center align-items-center mt-10">
										<div className="col">
											<input
												type="password"
												className="form-control register-control"
												placeholder="Password"
												value={this.state.password}
												onChange={e =>
													this.setState({ password: e.target.value })
												}
											/>
										</div>
									</div>
									<div className="row justify-content-center align-items-center mt-10">
										<div className="col">
											<input
												type="password"
												className="form-control register-control"
												placeholder="Confirm Password"
												value={this.state.confirmpass}
												onChange={e =>
													this.setState({ confirmpass: e.target.value })
												}
											/>
										</div>
									</div>
									<div className="row justify-content-start align-items-start mt-20">
										<div className="col">
											<button
												type="submit"
												className="btn btn-primary btn-lg btn-register"
												disabled={this.state.isLoading}
											>
												{this.state.isLoading ? 'Registering...' : 'Register'}
											</button>
										</div>
									</div>
								</form>
							</div>
						</Animated>
					</div>
				</div>
			</div>
		);
	}
}

export default Register;
