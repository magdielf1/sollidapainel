import React, { Component } from 'react';
import firebaseApp from '../../modulos/firebase/config';
import { Animated } from 'react-animated-css';

import './assets/css/forgot.css';
// import { hashHistory } from 'react-router';

class Forgot extends Component {
	state = {
		email: '',
		status: '',
		block: 'block'
	};

	recovery() {
		let _this = this;
		let emailorUsername = this.state.email;
		firebaseApp
			.auth()
			.sendPasswordResetEmail(emailorUsername)
			.then(() => {
				_this.setState({
					status:
						'A recovery email has been sent to your inbox. Check your inbox for more information.',
					block: 'none'
				});
			})
			.catch(function(error) {
				_this.setState({
					status: error.message
				});
			});
	}
	render() {
		const { history } = this.props.route;
		return (
			<div className="container-fluid">
				<div className="row justify-content-center align-items-center">
					<div className="col-xl-4 col-lg-4 col-md-4 col-sm-1">
						<Animated
							animationIn="fadeIn"
							animationOut="fadeOut"
							isVisible={true}
						>
							<div className="formCard">
								<div className="row justify-content-center align-items-center mt-40">
									<div className="logo">
										<img
											alt=""
											src="./images/logo.png"
											style={{ width: '100%' }}
										/>
									</div>
								</div>
								<div className="row" style={{ display: this.state.block }}>
									<div className="col">
										<h4 className="forgotlabel mt-40">Forgot Yout Password?</h4>
									</div>
								</div>
								<div
									className="row justify-content-center align-items-center mt-20"
									style={{ display: this.state.block }}
								>
									<div className="col">
										<input
											type="email"
											className="form-control forgot-control"
											placeholder="Your E-mail"
											value={this.state.email}
											onChange={e => this.setState({ email: e.target.value })}
										/>
									</div>
								</div>
								<div
									className="row justify-content-start  align-items-start mt-20"
									style={{ display: this.state.block }}
								>
									<div className="col">
										<button
											className="btn btn-lg btn-forgot"
											onClick={() => this.recovery()}
										>
											Reset Password
										</button>
									</div>
								</div>
								<p>{this.state.status}</p>
							</div>
						</Animated>
					</div>
				</div>
			</div>
		);
	}
}

export default Forgot;
