import React, { Component } from 'react';
import './assets/css/app.css';

class App extends Component {
	render() {
		return (
			<div className="App app-container">
				{/* <h1>HEADER</h1> */}
				{this.props.children}
			</div>
		);
	}
}

export default App;
